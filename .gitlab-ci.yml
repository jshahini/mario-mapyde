stages:
  - prebuild
  - build
  - prepare
  - generation
  - simulation
  - analysis

# Push image to latest in docker-registry
# If tagged, CI_COMMIT_REF_SLUG is the tag name with dashes
# IMAGE_NAME comes from the CI_JOB_NAME variable (build_xyz -> IMAGE_NAME=xyz)
.dockerfile_changed:
  only:
    refs:
      - master
    changes:
      - Dockerfiles/*

generate_build_images:
  extends: .dockerfile_changed
  stage: prebuild
  image: python:3.7-alpine
  before_script:
    - apk add git
  script:
    - python ci/generateYaml_dockerBuild.py $(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA --diff-filter=ACMR Dockerfiles/ | tr '\n' ' ')
                                            --output-file .build_images.yml
    - cat .build_images.yml
  artifacts:
    paths:
      - .build_images.yml

build_docker_images:
  extends: .dockerfile_changed
  stage: build
  trigger:
    include:
      - artifact: .build_images.yml
        job: generate_build_images
    strategy: depend # wait for child jobs to finish

prepare_inputs:
  image: python:3.7-alpine
  stage: prepare
  script:
    - python3 -m pip install -r requirements.txt
    - >
        scripts/mg5creator.py -P cards/process/charginos \
                              -r cards/run/default_LO.dat \
                              -p cards/param/Higgsino.slha \
                              -y pythia8_card.dat \
                              -m MN1 150.0 \
                              -m MN2 155.0 \
                              -m MC1 155.0 \
                              -E 100000 \
                              -n 1000 \
                              -c 1 \
                              -t gitlabci
  artifacts:
    name: "$CI_COMMIT_REF_NAME-inputs"
    paths:
      - output
    expire_in: 1 week

run_madgraph:
  image:
    #name: ${CI_REGISTRY_IMAGE}/madgraph:master
    name: gitlab-registry.cern.ch/scipp/mario-mapyde/madgraph:master
    entrypoint: [""]
  stage: generation
  dependencies: [prepare_inputs]
  before_script:
    - python3 -m pip install -r requirements.txt
    - ln -s ${CI_PROJECT_DIR}/cards /cards
    - ln -s ${CI_PROJECT_DIR}/output/gitlabci /data
    - mkdir /output && cd /output
  script:
    - mg5_aMC /data/run.mg5
    - rsync -rav PROC_madgraph /data/madgraph
  artifacts:
    name: "$CI_COMMIT_REF_NAME-madgraph"
    paths:
      - output
    expire_in: 1 week

run_delphes:
  image:
    #name: ${CI_REGISTRY_IMAGE}/delphes:master
    name: gitlab-registry.cern.ch/scipp/mario-mapyde/delphes:master
    entrypoint: [""]
  stage: simulation
  dependencies: [run_madgraph]
  before_script:
    - ln -s ${CI_PROJECT_DIR}/cards /cards
    - ln -s ${CI_PROJECT_DIR}/output/gitlabci /data
    - mkdir /output && cd /output
  script:
    - cp $(find /data/ -name "*hepmc.gz") hepmc.gz
    - gunzip hepmc.gz
    - /usr/local/share/delphes/delphes/DelphesHepMC /cards/delphes/delphes_card_ATLAS.tcl delphes.root hepmc
    - rsync -rav --exclude hepmc . /data/delphes
  artifacts:
    name: "$CI_COMMIT_REF_NAME-delphes"
    paths:
      - output
    expire_in: 1 week

run_analysis:
  image:
    #name: ${CI_REGISTRY_IMAGE}/delphes:master
    name: gitlab-registry.cern.ch/scipp/mario-mapyde/delphes:master
    entrypoint: [""]
  stage: analysis
  dependencies: [run_delphes]
  before_script:
    - ln -s ${CI_PROJECT_DIR}/cards /cards
    - ln -s ${CI_PROJECT_DIR}/scripts /scripts
    - ln -s ${CI_PROJECT_DIR}/output/gitlabci /data
    - mkdir /output && cd /output
  script:
    - /scripts/SimpleAna.py --input /data/delphes/delphes.root --output histograms.root
    - rsync -rav . /data/analysis
  artifacts:
    name: "$CI_COMMIT_REF_NAME-analysis"
    paths:
      - output
    expire_in: 1 week
