#!/bin/bash
set -e # exit when any command fails

tag=${1:-"test_Higgsino_001"}
delphescard=${2:-"delphes_card_ATLAS.tcl"}
cores=${3:-"1"}
clobber_delphes=${4:-"false"}
base=${PWD}
useDocker=${5:-"false"}
database=${6:-${PWD}/data}
datadir=${tag}

# first check if delphes output is already there.  If so, then don't clobber it unless told to.
if [[ -e ${database}/${datadir}/delphes && $clobber_delphes != true ]]; then
    echo "Delphes area in ${datadir} already exists, not running job.  Remove or rename it, or force clobbering."
    exit 0
fi

set -x

if [[ ${useDocker} == true ]]; then
#        --user $(id -u):$(id -g) \
    echo "Running with Docker"
    docker run \
	--name "${tag}__delphes" \
	--user $(id -u):$(id -g) \
	--rm \
	-v ${base}/cards:/cards \
	-v ${database}/${datadir}:/data \
	-w /tmp \
	--env delphescard=${delphescard} \
	gitlab-registry.cern.ch/scipp/mario-mapyde/delphes-snowmass:master \
	'set -x && \
        cp $(find /data/ -name "*hepmc.gz") hepmc.gz && \
        gunzip hepmc.gz && \
	/bin/ls -ltrh --color && \
        /usr/local/share/delphes/delphes/DelphesHepMC3 /cards/delphes/${delphescard} delphes.root hepmc && \
        rsync -rav --exclude hepmc . /data/delphes'
else
     echo "Running with Singularity"
     singularity exec \
	 -B ${base}/cards:/cards \
	 -B ${database}/${datadir}:/data \
	 -B /cvmfs \
	 -B /$(echo "$PWD" | cut -d "/" -f2) \
	 -W /tmp \
	 --env delphescard=${delphescard} \
	 docker://gitlab-registry.cern.ch/scipp/mario-mapyde/delphes-snowmass:master \
	 bash -c \
	 'mkdir -p delphes_tmp && \
         cd delphes_tmp && \
         set -x && \
         cp $(find /data/ -name "*hepmc.gz") hepmc.gz && \
         gunzip hepmc.gz && \
         /bin/ls -ltrh --color && \
         /usr/local/share/delphes/delphes/DelphesHepMC3 /cards/delphes/${delphescard} delphes.root hepmc && \
         rsync -rav --exclude hepmc . /data/delphes && \
         cd ../ && \
         rm -rf delphes_tmp'
fi
