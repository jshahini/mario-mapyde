#!/bin/bash
set -e # exit when any command fails

# defaults
ecms=100
mass=150
dM=5
proc=charginos
cores=20
nevents=1000
params=Higgsino
mmjj=500
mmjjmax=-1
deltaeta=3.0
ptj=20
ptj1min=0
suffix=""
skip_mgpy=false
skip_delphes=false
skip_ana=true
clobber_mgpy=false
clobber_delphes=false
clobber_ana=false
database=${PWD}/data
ktdurham="-1"
xqcut="-1"
seed=0
pythia_card="pythia8_card.dat"
base=${PWD}
MGversion="-2.9"
useDocker=false

while getopts "E:M:P:p:N:m:x:e:c:GgB:b:S:y:k:sd:j:J:X:I:D" opt; do
    case "${opt}" in
	E) ecms=$OPTARG;;
	M) mass=$OPTARG;;
	P) proc=$OPTARG;;
	p) params=$OPTARG;;
	N) nevents=$OPTARG;;
	m) mmjj=$OPTARG;;
	x) mmjjmax=$OPTARG;;
	e) deltaeta=$OPTARG;;
	c) cores=$OPTARG;;
	g) clobber_mgpy=true;;
	B) base=$OPTARG;;
	b) database=$OPTARG;;
	j) ptj=$OPTARG;;
	J) ptj1min=$OPTARG;;
	S) dM=$OPTARG;;
	y) pythia_card=$OPTARG;;
	k) ktdurham=$OPTARG;;
	X) xqcut=$OPTARG;;
	s) slepton=true;;
	d) seed=$OPTARG;;
	I) MGversion=$OPTARG;;
	D) useDocker=true;;
	\?) echo "Invalid option: -$OPTARG";;
    esac
done

shift $(($OPTIND - 1))
tag=${1:-"test_Higgsino_001"}
datadir=${tag}
mkdir -p ${database}

mN1=$(bc <<< "scale=2; ${mass}-${dM}")

if [[ ${slepton} == true ]]; then
    massopts="-m MSLEP ${mass}"
elif [[ ${params} == WinoBino ]]; then
    massopts="-m MN2 ${mass} -m MC1 ${mass}"
elif [[ ${params} == Higgsino ]]; then
    mC1=$(bc <<< "scale=2; ${mass}-${dM}/2")
    massopts="-m MN2 ${mass} -m MC1 ${mC1}"
fi

if [[ $xqcut != 0 && $xqcut != -1 ]]; then
    xqcuttmp="${xqcut} -R ickkw 1"
    xqcut=${xqcuttmp}
fi

./scripts/mg5creator.py \
    -o ${database} \
    -P cards/process/${proc} \
    -r cards/run/default_LO.dat \
    -p cards/param/${params}.slha \
    -y ${pythia_card} \
    -m MN1 ${mN1} ${massopts} \
    -R ptj ${ptj} -R ptj1min ${ptj1min} -R deltaeta ${deltaeta} -R mmjj ${mmjj} -R mmjjmax ${mmjjmax} -R ktdurham ${ktdurham} -R xqcut ${xqcut} \
    -c ${cores} \
    -E ${ecms}000 \
    -n ${nevents} \
    -s ${seed} \
    -t ${tag}

exit_status=$? 


if [[ ${useDocker} == true ]]; then
    if [[ ${exit_status} == 0 || ${clobber_mgpy} == true ]]; then
	echo "Running with Docker"
	docker run \
            --name "${tag}__mgpy" \
            --rm \
            --user $(id -u):$(id -g) \
            -v ${base}/cards:/cards \
            -v ${database}/${datadir}:/data \
            -w /tmp \
            gitlab-registry.cern.ch/scipp/mario-mapyde/madgraph${MGversion}:master \
            "mg5_aMC /data/run.mg5 && rsync -a PROC_madgraph /data/madgraph"
    fi

else
    if [[ ${exit_status} == 0 || ${clobber_mgpy} == true ]]; then
	echo "Running with Singularity"
        singularity exec \
            -B ${base}/cards:/cards \
            -B ${database}/${datadir}:/data \
            -B /cvmfs \
	    -B /$(echo "$PWD" | cut -d "/" -f2) \
            -W /tmp \
            docker://gitlab-registry.cern.ch/scipp/mario-mapyde/madgraph${MGversion}:master \
            bash -c \
            "export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/ &&
            mg5_aMC /data/run.mg5 &&
            rsync -a PROC_madgraph ${database}/${datadir}/madgraph &&
            rm -rf PROC_madgraph"
    fi
fi
