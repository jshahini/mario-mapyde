#!/bin/bash
#set -e # exit when any command fails

# defaults
ecms=13
mass=150
dM=1
proc=charginos
params=Higgsino
nevents=1000
mmjj=0
mmjjmax=-1
deltaeta=0.0
ptj=20
ptj1min=0
skip_mgpy=false
skip_delphes=false
skip_ana=true
skip_SA=true
clobber_mgpy=false
clobber_delphes=false
clobber_ana=false
suffix=""
base=${PWD}
database=${PWD}/data
datadir=${tag}
MGversion="-2.9"
pythia_card="pythia8_card.dat"
delphescard="FCChh.tcl"
ktdurham=-1
xqcut=-1
seed=0
useDocker=false
cores=20
anascript="SimpleAna.py"
simpleanalysis="EwkCompressed2018"
likelihood="Higgsino_2L_bkgonly"
XSoverride=""
sleptonopts=""

# get command line options
while getopts "E:M:P:p:N:m:x:s:e:c:GDAglaB:b:j:J:S:y:k:d:C:iL:f:F:X:h:I:nH" opt; do
    case "${opt}" in
	E) ecms=$OPTARG;;
	M) mass=$OPTARG;;
	P) proc=$OPTARG;;
	p) params=$OPTARG;;
	N) nevents=$OPTARG;;
	m) mmjj=$OPTARG;;
	x) mmjjmax=$OPTARG;;
	s) suffix=$OPTARG;;
	e) deltaeta=$OPTARG;;
	c) cores=$OPTARG;;
	G) skip_mgpy=true;;
	D) skip_delphes=true;;
	A) skip_ana=true;;
	g) clobber_mgpy=true;;
	l) clobber_delphes=true;;
	a) clobber_ana=true;;
	B) base=$OPTARG;;
	b) database=$OPTARG;;
	j) ptj=$OPTARG;;
	J) ptj1min=$OPTARG;;
	S) dM=$OPTARG;;
	y) pythia_card=$OPTARG;;
	k) ktdurham=$OPTARG;;
	d) seed=$OPTARG;;
	C) anascript=$OPTARG;;
	i) skip_SA=false;;
	L) delphescard=$OPTARG;;
	f) simpleanalysis=$OPTARG;;
	F) likelihood=$OPTARG;;
	X) xqcut=$OPTARG;;
	h) XSoverride=$OPTARG;;
	I) MGversion=$OPTARG;;
	n) sleptonopts="-s";;
	H) useDocker=true;;
	\?) echo "Invalid option: -$OPTARG";;
    esac
done

echo $anascript
echo $clobber_ana

# construct the tag.
tag="Charginos_${ecms}_${params}_${mass}_mmjj_${mmjj}_${mmjjmax}${suffix}"
if [[ $mmjj == 0.0 ]]; then
   tag="Charginos_${ecms}_${params}_${mass}_${dM}_${proc}_${suffix}"
fi

# run MadGraph+Pythia, using test script
if $skip_mgpy; then
    echo "Skipping Madgraph for this job."
else
    clobberopt=""
    if $clobber_mgpy; then
	clobberopt="-g"
    fi

    dockerOpt=""
    if $useDocker; then
	dockerOpt="-D"
    fi

    ./test/wrapper_mgpy.sh \
	-b ${database} \
	-P ${proc} \
	-p ${params} \
	-y ${pythia_card} \
	-S ${dM} \
	-M ${mass} \
	-m ${mmjj} \
	-x ${mmjjmax} \
	-e ${deltaeta} \
	-E ${ecms} \
	-c ${cores} \
	-k ${ktdurham} \
	-X ${xqcut} \
	-N ${nevents} \
	-d ${seed} \
	-j ${ptj} \
	-J ${ptj1min} \
	-I "${MGversion}" \
	${clobberopt} \
	${sleptonopts} \
	${dockerOpt} \
	${tag}
fi

# run Delphes, using test script
if $skip_delphes; then
    echo "Skipping delphes for this job."
else
    ./test/wrapper_delphes.sh ${tag} ${delphescard}  ${cores} ${clobber_delphes} ${useDocker}
fi
